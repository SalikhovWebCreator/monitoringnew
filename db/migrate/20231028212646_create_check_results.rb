# frozen_string_literal: true

class CreateCheckResults < ActiveRecord::Migration[7.0]
  def change
    create_table :check_results do |t|
      t.integer :errors_count
      t.references :project, null: false
      t.references :delayed_check, null: false
      t.timestamps
    end
  end
end

# frozen_string_literal: true

class CreateDelayedChecks < ActiveRecord::Migration[7.0]
  def change
    create_table :delayed_checks do |t|
      t.references :project, null: false
      t.boolean :active, default: false
      t.float :delay, null: false
      t.string :check_type, null: false
      t.string :email, null: false

      t.timestamps
    end
  end
end

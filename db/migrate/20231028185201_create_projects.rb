# frozen_string_literal: true

class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.string :project_url, null: false, index: true
      t.boolean :checkable, default: false
      t.references :user
      t.timestamps
    end
  end
end

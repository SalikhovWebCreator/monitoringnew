# frozen_string_literal: true

class CreatePages < ActiveRecord::Migration[7.0]
  def change
    create_table :pages do |t|
      t.string :page_url, null: false, index: true
      t.references :project

      t.timestamps
    end
  end
end

class AddTitleToProjects < ActiveRecord::Migration[7.0]
  def change
    add_column :projects, :title, :string, null: false, default: 'Project'
    #Ex:- add_column("admin_users", "username", :string, :limit =>25, :after => "email")
  end
end

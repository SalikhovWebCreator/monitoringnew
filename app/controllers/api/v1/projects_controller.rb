# frozen_string_literal: true

module Api
  module V1
    class ProjectsController < Api::V1::ApiController
      # temporary
      before_action :current_user
      skip_before_action :authenticate_user!
      before_action :find_project, only: %i[update show destroy]
      # before_action :check_project_owner, except: [:index, :create]

      def index
        render json: @current_user.projects, each_serializer: ProjectSerializer
      end

      def show
        render json: @project, serializer: ProjectSerializer
      end

      def create
        project = @current_user.projects.new(project_params)
        if project.save!
          render json: project, serializer: ProjectSerializer
        else 
          render json: project.errors
        end
      end

      def update
        if @project.update!(project_params)
          render json: @project, serializer: ProjectSerializer
        else 
          render json: @project.errors
        end
      end

      def destroy
        return unless @project.destroy

        render json: @current_user.projects, each_serializer: ProjectSerializer
      end

      def upload_project
        puts params
        file = params
      
        project_url = 'https://' + file["item"].first["request"]["url"].split('/')[2]
        project = Project.find_by(project_url: project_url)

        if project
          file["item"].each do |file_item|
            url = file_item["request"]["url"]
            next if url.class != String
            project.pages.find_or_create_by(page_url: url, method: file_item["request"]["method"])
          end

          render json: project, serializer: ProjectSerializer
        else
          project = Project.new(user: User.last, title: file["info"]["name"], 
                                project_url: project_url, checkable: false)

          if project.save!
            file["item"].each do |file_item|
              url = file_item["request"]["url"]
              next if url.class != String
              project.pages.find_or_create_by(page_url: url, method: file_item["request"]["method"])
            end

            render json: project, serializer: ProjectSerializer
          end
        end
      end

      private

      def current_user
        @current_user = User.last
      end

      def check_project_owner
        head :forbidden unless project.user == current_user
      end

      def find_project
        @project = Project.find(params[:id])
      end

      def project_params
        params.permit(:id, :project_url, :title, :checkable, pages_attributes: %i[id page_url _destroy],
                                                delayed_checks_attributes: %i[id active delay check_type email _destroy])
      end
    end
  end
end

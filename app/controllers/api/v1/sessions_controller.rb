# frozen_string_literal: true

module Api
  module V1
    class SessionsController < DeviseTokenAuth::SessionsController
      skip_before_action :verify_authenticity_token
      include Api::Concerns::ActAsApiRequest

      private

      def resource_params
        params.permit(:email, :password)
      end

      def render_create_success
        render :create
      end
    end
  end
end

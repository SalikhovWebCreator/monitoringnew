# frozen_string_literal: true

module Api
  module V1
    class RegistrationsController < DeviseTokenAuth::RegistrationsController
      skip_before_action :verify_authenticity_token
      include Api::Concerns::ActAsApiRequest

      private

      def sign_up_params
        params.permit(:email, :password, :password_confirmation,
                      :username, :first_name, :last_name)
      end

      def render_create_success
        render :create
      end
    end
  end
end

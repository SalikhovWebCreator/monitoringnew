# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id          :bigint           not null, primary key
#  project_url :string           not null
#  checkable   :boolean          default(FALSE)
#  user_id     :bigint
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  title       :string           default("Project"), not null
#
# Indexes
#
#  index_projects_on_project_url  (project_url)
#  index_projects_on_user_id      (user_id)
#
class ProjectSerializer < ActiveModel::Serializer
  attributes %i[project_url id title checkable pages delayed_checks ]

  def delayed_checks
    object.delayed_checks.map {|check| check.check_results.map {|check_result| {file: Rails.application.routes.url_helpers.rails_blob_path(check_result.result_file, disposition: 'attachment'), 
                                                                                errors_count: check_result.errors_count}}}
  end
end

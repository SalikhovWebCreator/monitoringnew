# frozen_string_literal: true

# == Schema Information
#
# Table name: delayed_checks
#
#  id         :bigint           not null, primary key
#  project_id :bigint           not null
#  active     :boolean          default(FALSE)
#  delay      :float            not null
#  check_type :string           not null
#  email      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_delayed_checks_on_project_id  (project_id)
#
class DelayedCheck < ApplicationRecord
  belongs_to :project
  has_many :check_results

  validates :delay, :check_type, :email, presence: true

  after_update :toggle_job
  after_create :run_job

  def toggle_job
    if active? && active_changed?
      CheckForAvailiabilityJob.perform_later(self)
    end
  end

  def run_job
    if active? 
      CheckForAvailiabilityJob.perform_later(self)
    end
  end
end

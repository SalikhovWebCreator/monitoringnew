# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id          :bigint           not null, primary key
#  project_url :string           not null
#  checkable   :boolean          default(FALSE)
#  user_id     :bigint
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  title       :string           default("Project"), not null
#
# Indexes
#
#  index_projects_on_project_url  (project_url)
#  index_projects_on_user_id      (user_id)
#
class Project < ApplicationRecord
  validates :project_url, :title, presence: true, uniqueness: true
  validate :url_format

  belongs_to :user

  has_many :pages, dependent: :destroy
  has_many :check_results, dependent: :destroy
  has_many :delayed_checks,  dependent: :destroy

  accepts_nested_attributes_for :pages, allow_destroy: true
  accepts_nested_attributes_for :delayed_checks, allow_destroy: true

  def url_format
    url = begin
      URI.parse(project_url)
    rescue StandardError
      false
    end
    errors.add(:link, 'Not valid link') unless url.is_a?(URI::HTTP) || url.is_a?(URI::HTTPS)
  end
end

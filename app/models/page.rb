# frozen_string_literal: true

# == Schema Information
#
# Table name: pages
#
#  id         :bigint           not null, primary key
#  page_url   :string           not null
#  project_id :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  method     :string
#
# Indexes
#
#  index_pages_on_page_url    (page_url)
#  index_pages_on_project_id  (project_id)
#
class Page < ApplicationRecord
  validates :page_url, presence: true, uniqueness: true
  validate :url_format

  belongs_to :project

  def url_format
    url = begin
      URI.parse(page_url)
    rescue StandardError
      false
    end
    errors.add(:link, 'Not valid link') unless url.is_a?(URI::HTTP) || url.is_a?(URI::HTTPS)
  end
end

class CheckForAvailiabilityJob < ApplicationJob
  def perform(delayed_check)
      if delayed_check.active?
          project = delayed_check.project
          project_id = project.id
          form_csv(check_pages(project.pages), delayed_check)

          CheckForAvailiabilityJob.set(wait: delayed_check.delay.minutes).perform_later(delayed_check)
      end
  end
  
  def check_pages(pages)
    result_array = []
    @errors_count = 0

    pages.each do |page|
      response = RestClient.get page.page_url

      result_array << if [200, 201, 302, 304].include?(response.code)
                        [page.page_url, 'Status OK']
                      else
                        [page.page_url, "Status #{response.code} ERROR"]
                        @errors_count += 1
                      end
    end

    result_array
  rescue RestClient::Exception => e
    puts e
  end
  
  def form_csv(result_array, delayed_check)
    return if result_array.nil?

    file = File.new("#{Rails.root}/public/result.csv", 'w')
    CSV.open("#{Rails.root}/public/result.csv", 'w') do |csv|
      result_array.each do |row|
        csv << row
      end
    end

    delayed_check.check_results.create(project_id: delayed_check.project_id, errors_count: @errors_count)
                      .result_file.attach(io: File.open(file.path), filename: "check-for-availiability-#{Date.today.strftime}.csv")

    File.delete(file.path)
  end
end
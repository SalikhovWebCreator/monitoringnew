# frozen_string_literal: true

# == Schema Information
#
# Table name: pages
#
#  id         :bigint           not null, primary key
#  page_url   :string           not null
#  project_id :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  method     :string
#
# Indexes
#
#  index_pages_on_page_url    (page_url)
#  index_pages_on_project_id  (project_id)
#
require 'rails_helper'

RSpec.describe Page do
  pending "add some examples to (or delete) #{__FILE__}"
end

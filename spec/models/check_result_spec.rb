# frozen_string_literal: true

# == Schema Information
#
# Table name: check_results
#
#  id               :bigint           not null, primary key
#  errors_count     :integer
#  project_id       :bigint           not null
#  delayed_check_id :bigint           not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_check_results_on_delayed_check_id  (delayed_check_id)
#  index_check_results_on_project_id        (project_id)
#
require 'rails_helper'

RSpec.describe CheckResult do
  pending "add some examples to (or delete) #{__FILE__}"
end

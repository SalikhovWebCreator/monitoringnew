# frozen_string_literal: true

# == Schema Information
#
# Table name: delayed_checks
#
#  id         :bigint           not null, primary key
#  project_id :bigint           not null
#  active     :boolean          default(FALSE)
#  delay      :float            not null
#  check_type :string           not null
#  email      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_delayed_checks_on_project_id  (project_id)
#
require 'rails_helper'

RSpec.describe DelayedCheck do
  pending "add some examples to (or delete) #{__FILE__}"
end

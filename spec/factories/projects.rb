# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id          :bigint           not null, primary key
#  project_url :string           not null
#  checkable   :boolean          default(FALSE)
#  user_id     :bigint
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  title       :string           default("Project"), not null
#
# Indexes
#
#  index_projects_on_project_url  (project_url)
#  index_projects_on_user_id      (user_id)
#
FactoryBot.define do
  factory :project do
  end
end

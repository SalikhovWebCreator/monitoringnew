require 'swagger_helper'

RSpec.describe 'api/v1/projects', type: :request do
  path '/api/v1/projects' do
    get('list projects') do
      response(200, 'successful') do
        produces 'application/json'

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test!
      end
    end

    post('create project') do
      response(200, 'successful') do
        consumes 'application/json'

        parameter name: :project, in: :body, schema: {
          type: :object,
          properties: {
            project_url: { type: :string },
            checkable: { type: :boolean },
            title: { type: :string },
            pages_attributes: { type: :array,
                                items: {
                                  type: :object,
                                  properties: {
                                    page_url: { type: :string }
                                  }
                                } },
            delayed_checks_attributes: { type: :array,
                                         items: {
                                           type: :object,
                                           properties: {
                                             active: { type: :boolean },
                                             delay: { type: :float },
                                             check_type: { type: :string },
                                             email: { type: :string },
                                             _destroy: { type: :boolean }
                                           }
                                         } }
          },
          required: %w[project_url checkable]
        }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test!
      end
    end
  end

  path '/api/v1/upload_project' do
    post('create project') do
      response(200, 'successful') do
        consumes 'application/json'

        parameter name: :file, in: :formData, type: :file

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test!
      end
    end
  end

  path '/api/v1/projects/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :integer, description: 'id'

    get('show project') do
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test!
      end
    end

    patch('update project') do
      response(200, 'successful') do
        let(:id) { '123' }
        consumes 'application/json'

        parameter name: :project, in: :body, schema: {
          type: :object,
          properties: {
            project_url: { type: :string },
            checkable: { type: :boolean },
            title: { type: :string },
            pages_attributes: { type: :array,
                                items: {
                                  type: :object,
                                  properties: {
                                    id: { type: :integer },
                                    page_url: { type: :string },
                                    _destroy: { type: :boolean }
                                  }
                                } },
            delayed_checks_attributes: { type: :array,
                                         items: {
                                           type: :object,
                                           properties: {
                                             id: { type: :integer },
                                             active: { type: :boolean },
                                             delay: { type: :float },
                                             check_type: { type: :string },
                                             email: { type: :string },
                                             _destroy: { type: :boolean }
                                           }
                                         } }
          },
          required: %w[project_url checkable]
        }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test!
      end
    end

    delete('delete project') do
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end

        run_test!
      end
    end
  end
end

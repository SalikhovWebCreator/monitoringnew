require 'swagger_helper'

RSpec.describe 'api/v1/registrations', type: :request do

  path '/api/v1/users' do

    post('new registration') do
      response(200, 'successful') do
        consumes 'application/json'

        parameter name: :user, in: :body, schema: {
            type: :object,
            properties: {
              email: { type: :string },
              password: { type: :string },
              password_confirmation: { type: :string },
              username: { type: :string },
              first_name: { type: :string },
              last_name: { type: :string },
            },
            required: %w[email password password_confirmation username first_name last_name]
        }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
